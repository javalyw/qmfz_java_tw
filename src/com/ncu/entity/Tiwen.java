package com.ncu.entity;


import java.util.Date;

public class Tiwen {
	private String openID;
	private String xiaoqu;
	private String shuyuan;
	private String xingming;
	private String banji;
	private String tiwen;
	private Date createTime;
	
	
	public String getOpenID() {
		return openID;
	}
	public void setOpenID(String openID) {
		this.openID = openID;
	}
	
	public String getXiaoqu() {
		return xiaoqu;
	}
	public void setXiaoqu(String xiaoqu) {
		this.xiaoqu = xiaoqu;
	}
	public String getShuyuan() {
		return shuyuan;
	}
	public void setShuyuan(String shuyuan) {
		this.shuyuan = shuyuan;
	}
	public String getXingming() {
		return xingming;
	}
	public void setXingming(String xingming) {
		this.xingming = xingming;
	}
	public String getBanji() {
		return banji;
	}
	public void setBanji(String banji) {
		this.banji = banji;
	}
	public String getTiwen() {
		return tiwen;
	}
	public void setTiwen(String tiwen) {
		this.tiwen = tiwen;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
}
