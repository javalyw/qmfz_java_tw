// pages/tiwena/tiwena.js
const app = getApp()
Page({





  data: {
    text: '请填完再提交！（勿重复提交）',
    duration: 0, //水平滚动方法一中文字滚动总时间
    pace: 1,  //滚动速度
    posLeft1: 0,  //水平滚动left值
    oneItems: ["明德书院","崇礼书院","励能书院","笃学书院","思诚书院"],//校区
    oneItemsID:1
  },
 
  onShow: function () {
    let that = this;
    let windowWidth = wx.getSystemInfoSync().windowWidth; //屏幕宽度
    wx.createSelectorQuery().select('#txt1').boundingClientRect(function (rect) {
      let duration = rect.width * 0.03;//滚动文字时间,滚动速度为0.03s/px
      that.setData({
        duration: duration
      })
    }).exec()
  },


  selectorChange:function(e){//picker
    var i =e.detail.value;
    var value =this.data.oneItems[i];
    this.setData({
      selector:value
    });
  },

  onPullDownRefresh: function () {
    var that = this;
    that.setData({
      currentTab: 0 //当前页的一些初始数据，视业务需求而定
    })
    this.onLoad(); //重新加载onLoad()
  },
  onLoad: function (options) { 
    wx.stopPullDownRefresh() //刷新完成后停止下拉刷新动效
  },
  formSubmit: function (e) { //    
    //console.log(e.detail.value);
    var that = this;
    var userid = wx.getStorageSync('openid');
    if (e.detail.value.xiaoqu.length == 0) {
       wx.showToast({
       title: '校区不能为空!',
       icon: 'loading',
       duration: 1500
      })
      setTimeout(function () {
        wx.hideToast()
      }, 2000)
    } else if (e.detail.value.shuyuan.length == 0 ) {
      wx.showToast({
        title: '书院不能为空!',
        icon: 'loading',
        duration: 1500
       })    
    }else if (e.detail.value.xingming.length == 0 || e.detail.value.xingming.length >= 8) {
      wx.showToast({
        itle: '姓名不能为空或过长!',
        con: 'loading',
        duration: 1500
      })
      setTimeout(function () {
        wx.hideToast()
       }, 2000)
    }else if (e.detail.value.tiwen.length == 0) {
      wx.showToast({
        title: '体温不能为空!',
        icon: 'loading',
        duration: 1500
      })
      setTimeout(function () {
        wx.hideToast()
      }, 2000)
    }else if(e.detail.value.banji.length == 0) {
      wx.showToast({
        title: '班级不能为空!',
        icon: 'loading',
        duration: 1500
      })
      setTimeout(function () {
        wx.hideToast()
      }, 2000)
    } else {
      wx.request({
        url: 'http://localhost:8080/SignIn/Process',
        data: { 
          openID: userid,
          xiaoqu: e.detail.value.xiaoqu, 
          shuyuan: e.detail.value.shuyuan, 
          xingming: e.detail.value.xingming,
          banji: e.detail.value.banji,
          tiwen:e.detail.value.tiwen,
          flag: 'tiwen'
        },
          method: "POST",
          header: {"Content-Type": "application/x-www-form-urlencoded"},
        success: function (res) {
          console.log(res.data);
          if (res.data.status == 0) {
            wx.showToast({
              title: '提交失败！！！',
              icon: 'loading',
              duration: 1500
            })
            that.setData({
              xingming:'',
              banji:'',
              tiwen:''
            })  
          } else {
            wx.showToast({
              title: '提交成功！！！',//这里打印出登录成功
              icon: 'success',
              duration: 1000
            })
            that.setData({
              xingming:'',
              banji:'',
              tiwen:''
            }) 
            
          }
         /*  wx.reLaunch({
            title: '提交成功！！！',//这里打印出登录成功
            icon: 'success',
            duration: 2000,
            url: '../tiwena/tiwena',
            
          }) */
        },
        
      })
    }
    
  },
  
 
})